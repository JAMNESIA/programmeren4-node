const http = require('http')

//const hostname = '127.0.0.1'
const port = process.env.PORT || 3000

const server = http.createServer(function (req, res){
    console.log('er was een request!')
    const result = {
        status: 'Alles oke',
        name: 'Hello World'
    }
  res.statusCode = 200
  res.setHeader('Content-Type', 'application/json')
  res.end(JSON.stringify(result))
})

server.listen(port, function(){
  console.log(`Server running at port ${port}`)
})